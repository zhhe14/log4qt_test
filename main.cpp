﻿//#include <QCoreApplication>
//#include "logger.h"
//#include "propertyconfigurator.h"
//#include "basicconfigurator.h"
//#include "logmanager.h"

//int main(int argc, char *argv[])
//{
//    QCoreApplication a(argc, argv);
//    //配置文件路径
//    Log4Qt::PropertyConfigurator::configure(a.applicationDirPath()
//    + "/log4qt.conf");
//    Log4Qt::Logger *a1 = Log4Qt::Logger::logger("A1");
//    a1->debug("the message of log");

//    Log4Qt::BasicConfigurator::configure();
//    Log4Qt::Logger * log = Log4Qt::Logger::rootLogger();
//    log->debug("debug!");
//    log->info("information!");
//    log->warn("warn");

//    Log4Qt::LogManager::setHandleQtMessages(true);
//    qDebug("Hello World!");
//    qWarning("Hello Qt!");
//    qErrnoWarning("Hello Error！");

//    return a.exec();
//}

//#include <QCoreApplication>
//#include <QDir>
//#include <log4qt/logger.h>
//#include <log4qt/basicconfigurator.h>
//#include <log4qt/patternlayout.h>
//#include <log4qt/dailyrollingfileappender.h>
//#include <log4qt/fileappender.h>

//#define FIX_DIR "C:/Users/hezhenhua/Desktop/"

//int main(int argc, char* argv[])
//{
//    QCoreApplication app(argc, argv);
//    Log4Qt::BasicConfigurator::configure();
//    //log pattern
//    Log4Qt::Logger *log = Log4Qt::Logger::rootLogger();
//    Log4Qt::PatternLayout *lay = new Log4Qt::PatternLayout(Log4Qt::PatternLayout::TTCC_CONVERSION_PATTERN);
//    lay->setConversionPattern("%-d{yyyy-MM-dd HH:mm:ss:zzz} [%c]-[%p] %m%n");
//    lay->activateOptions();
//    //log output
//    QString path = QString(FIX_DIR + QString("log4qt_output_test"));
//    //new output folder
//    QDir *dirtmp = new QDir();
//    dirtmp->mkpath(path);

//    Log4Qt::FileAppender *fileappender = new Log4Qt::FileAppender(lay, path + "/" + QDateTime::currentDateTime().toString("yyyyMMdd") +".log");
//    fileappender->setAppendFile(true);
//    fileappender->activateOptions();
//    log->addAppender(fileappender);

//    log->info("this is a test information ~");
//    log->error("this is a error ~");
//    log->warn("hey, this is a warning ~");
//    log->debug("debug info mation");

//    delete dirtmp;
//    log->deleteLater();
//    return app.exec();
//}

#include <QCoreApplication>
#include "qblog4qt.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

//    QBLog4Qt::instance()->debug("debug");
//    QBLog4Qt::instance()->info("debug");
//    QBLog4Qt::instance()->warn("debug");
//    QBLog4Qt::instance()->error("debug");
//    QBLog4Qt::instance()->fatal("debug");

    QString str = "String";
    const char * p = "char*";
    int i = 1;
    double d= 1.1;
    char c = 'c';
    float f = 1.111;

    Log4Info(str);
    Log4Info(str,p);
    Log4Info(str,p,i);
    Log4Info(str,p,i,d);
    Log4Info(str,p,i,d,c);
    Log4Info(str,p,i,d,c,f);
    Log4Info(str,p,i,d,c,f,"汉字");
    Log4Info(str,p,i,d,c,f,"汉字","log4qt!");

    Log4qt<<"1"<<1<<1.1<<'c'<<"汉字"<<"log4qt!";

    return a.exec();
}

